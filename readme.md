# QV2 Testing Suite

## Setup
---

Let's say you want to install the project at /var/www/quoretesting/

### Step 1 - Clone the [bitbucket repo](https://bitbucket.org/murribu/quoretesting)

```
$ cd /var/www
$ git clone https://bitbucket.org/murribu/quoretesting.git
```
   
### Step 2 - Install dependencies

```
$ cd /var/www/quoretesting
$ composer install
$ npm install
```

If you need help with these, visit [Composer](https://getcomposer.org/) and/or [npm](https://www.npmjs.com/)

### Step 3 - Checkout a branch from Quore's svn into the ```/var/www/quoretesting/public``` folder

```
$ svn checkout http://quids.quore/svn/[branch_name] /var/www/quoretesting/public
```
   
### Step 4 - Setup configuration variables
a. Copy your includes/config.php file into the repo.

b. Copy this file ```/var/www/quoretesting/.env.example``` to this location ```/var/www/quoretesting/.env``` and set the following values appropriately in the new ```.env``` file. 
   
```
APP_URL=http://localhost
DB_HOST=127.0.0.1
DB_DATABASE=db_name
DB_USERNAME=db_username
DB_PASSWORD=db_password
IMPERSONATOR=impersonating_user
IMPERSONATOR_PASSWORD=impersonating_users_password
```

c. The APP_URL setting should be the base_url for the site you're testing against.

d. Those last two settings should be a username and password that will be used to impersonate users during testing.

e. The DB_* settings should the same as your ```includes/config.php``` file.


## Writing Tests
---

Tests are found in the ```/var/www/quoretesting/tests/Browser``` folder.

Please add more tests! Use the existing files as examples, specifically LoginTest.php and InspectionTest.php


## Running Tests
---

To run all tests
```
$ cd /var/www/quoretesting
$ php artisan dusk
```

To run one test
```
$ cd /var/www/quoretesting
$ php artisan dusk tests/Browser/LoginTest.php
```

## Things to keep in mind
---
* If your test creates/edits/deletes information from the database, those changes will persist. That is to say, this testing suite does not ensure idempotency.

* You can point the APP_URL at Stage, EmergencyQA, etc. Obviously, this carries some risk with it. Use with caution.

* [Laravel Dusk](https://laravel.com/docs/5.4/dusk) has great documentation for assertions and using the Browser class.

* [Faker](https://github.com/fzaninotto/Faker) is a great resource for mocking up fake names, etc.

* If the tests are running very slowly, run this command in a separate terminal window ```Xvfb -ac :0 -screen 0 1280x1024x16 &```