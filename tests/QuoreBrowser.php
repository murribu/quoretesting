<?php
namespace Tests;

use Laravel\Dusk\Browser;

class QuoreBrowser extends Browser{
    public function loginAs($user, $guard = null){
        $username = gettype($user) == 'string' ? $user : $user->username;
        return $this->visit('/logout.php')
            ->visit('/login.php')
            ->type('username', '@'.$username)
            ->type('password', env('IMPERSONATOR').'|'.env('IMPERSONATOR_PASSWORD'))
            ->press('Sign In');
    }
}