<?php
namespace Tests\Browser;

use Tests\DuskTestCase;
use Tests\QuoreBrowser;
use Faker\Factory as Faker;

use App\InspectionTemplate;
use App\User;

class InspectionTest extends DuskTestCase
{
    protected $agm_user;
    protected $eng_user1;
    protected $eng_user2;
    protected $fd_user;
    protected $hk_user;
    protected $sales_user;
    protected $food_and_beverage_user;
    protected $regional_manager_user;
    
    protected $eng_inspection_info;
    
    public function setUp() {
        $faker = Faker::create();
        
        parent::setUp();
        $this->agm_user = User::where('username', 'bobagm')->first();
        
        $this->eng_user1 = User::where('primary_property_id', $this->agm_user->primary_property_id)
            ->where('active', '1')
            ->where('department_id', '2')
            ->inRandomOrder()
            ->first();
        $this->eng_user2 = User::where('primary_property_id', $this->agm_user->primary_property_id)
            ->where('id', '<>', $this->eng_user1->id)
            ->where('active', '1')
            ->where('department_id', '2')
            ->inRandomOrder()
            ->first();
        $this->fd_user = User::where('primary_property_id', $this->agm_user->primary_property_id)
            ->where('active', '1')
            ->where('department_id', '3')
            ->inRandomOrder()
            ->first();
        $this->hk_user = User::where('primary_property_id', $this->agm_user->primary_property_id)
            ->where('active', '1')
            ->where('department_id', '4')
            ->inRandomOrder()
            ->first();
        $this->sales_user = User::where('primary_property_id', $this->agm_user->primary_property_id)
            ->where('active', '1')
            ->where('department_id', '5')
            ->inRandomOrder()
            ->first();
        $this->food_and_beverage_user = User::where('primary_property_id', $this->agm_user->primary_property_id)
            ->where('active', '1')
            ->where('department_id', '8')
            ->inRandomOrder()
            ->first();
        $this->regional_manager_user = User::where('primary_property_id', $this->agm_user->primary_property_id)
            ->where('active', '1')
            ->where('department_id', '9')
            ->inRandomOrder()
            ->first();
            
        $this->eng_inspection_info = [
            'name'          => $faker->sentence(3),
            'description'   => $faker->paragraph
        ];
    }
    
    public function testSeeInspections(){
        $this->browse(function(QuoreBrowser $browser){
            
            $browser->loginAs($this->agm_user)
                ->visit('/inspection/hotel_inspections.php')
                ->assertSee('Inspections');
            
        });
    }
    
    /**
     * Tests accessibility for Engineering Inspection Templates.
     *
     * This test creates an Inspection Template as a GM and assigns it to the Engineering department
     * It verifies that that Template shows up in the "Start New Inspection" dropdown for 1s, 2s, and 9s - and nobody else
     * Finally, it deletes the new Inspection Template
     *
     * @return void
     * @author Cory Martin
     */
    
    public function testSeeEngInspection(){
        $this->browse(function(QuoreBrowser $first, QuoreBrowser $second){
            $first->loginAs($this->agm_user)
                ->visit('/inspection/inspections_template.php')
                ->type('name', $this->eng_inspection_info['name'])
                ->type('details', $this->eng_inspection_info['description'])
                ->select('iscorp', '12')
                ->press('Create Inspection');
                
            $first->loginAs($this->eng_user2)
                ->visit('/inspection/inspections_template.php')
                ->type('name', $this->eng_inspection_info['name'])
                ->type('details', $this->eng_inspection_info['description'])
                ->select('iscorp', '12')
                ->press('Create Inspection');
            
            $template_gm_eng = InspectionTemplate::where('name', $this->eng_inspection_info['name'])->first();
            
            $this->assertNotNull($template_gm_eng);
            
            // assert that the eng user can start the new Template
            $second->loginAs($this->eng_user1)
                ->visit('inspection/hotel_inspections.php')
                ->click('input[value="Start New Inspection"]')
                ->assertVisible("select[name='type'] option[value='".$template_gm_eng->id."']");
            
            // assert that the front desk user cannot start the new Template
            $second->loginAs($this->fd_user)
                ->visit('inspection/hotel_inspections.php')
                ->click('input[value="Start New Inspection"]')
                ->assertMissing("select[name='type'] option[value='".$template_gm_eng->id."']");
            
            // assert that the housekeeping user cannot start the new Template
            $second->loginAs($this->hk_user)
                ->visit('inspection/hotel_inspections.php')
                ->click('input[value="Start New Inspection"]')
                ->assertMissing("select[name='type'] option[value='".$template_gm_eng->id."']");
            
            // assert that the sales user cannot start the new Template
            $second->loginAs($this->sales_user)
                ->visit('inspection/hotel_inspections.php')
                ->click('input[value="Start New Inspection"]')
                ->assertMissing("select[name='type'] option[value='".$template_gm_eng->id."']");
            
            // assert that the F&B user cannot start the new Template
            $second->loginAs($this->food_and_beverage_user)
                ->visit('inspection/hotel_inspections.php')
                ->click('input[value="Start New Inspection"]')
                ->assertMissing("select[name='type'] option[value='".$template_gm_eng->id."']");
            
            // assert that the Regional Manager user can start the new Template
            $second->loginAs($this->regional_manager_user)
                ->visit('inspection/hotel_inspections.php')
                ->click('input[value="Start New Inspection"]')
                ->assertVisible("select[name='type'] option[value='".$template->id."']");
                
            // clean up your mess
            $first->visit('inspection/delete_inspection.php?id='.$template_gm_eng->id);
        });
    }
}
