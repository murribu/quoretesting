<?php
namespace Tests\Browser;

use Auth;
use Tests\DuskTestCase;
use Tests\QuoreBrowser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Faker\Factory as Faker;

use App\User;

class LoginTest extends DuskTestCase
{
    protected $user;
    protected $username;
    
    public function setUp() {
        parent::setUp();
        $this->username = 'bobagm';
        $this->user = User::where('username', $this->username)->first();
    }
    
    public function testLoginObject(){
        $this->browse(function(QuoreBrowser $browser){
            $browser->loginAs($this->user)
                ->assertSee('Dashboard');
        });
    }
    
    public function testLoginString(){
        $this->browse(function(QuoreBrowser $browser){
            $browser->loginAs($this->username)
                ->assertSee('Dashboard');
        });
    }
}