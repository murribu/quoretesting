<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Tests\QuoreBrowser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->browse(function (QuoreBrowser $browser) {
            $browser->visit('/login.php')
                    ->assertSee('Sign in to Quore');
        });
    }
}
