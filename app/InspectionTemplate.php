<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class InspectionTemplate extends Model{
    public $table = 'insp_template';
    
    public $timestamps = false;
}
